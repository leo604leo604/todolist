import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit, Renderer, Directive } from '@angular/core';
import { TodoService } from '../service/todo.service';

@Component({
  selector: 'app-my-task',
  templateUrl: './my-task.component.html',
  styleUrls: ['./my-task.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        overflow: 'hidden',
        height: '*',
        width: '690px'
      })),
      state('out', style({
        opacity: '0',
        overflow: 'hidden',
        height: '0px',
        width: '0px'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('Change', [
      state('bring', style({
        background: '#FFF2DC',
      })),
      state('dark', style({
        background: '#F2F2F2 ',
      })),
      transition('bring => dark', animate('400ms ease-in-out')),
      transition('dark => bring', animate('400ms ease-in-out'))
    ])
  ]
})

export class MyTaskComponent implements OnInit {
  Title: String;
  DeadLine: Date;
  Comment: String;
  todoList: any[];
  constructor(private todoService: TodoService, private render: Renderer) { }

  ngOnInit() {

    this.todoService.GetTodoList().snapshotChanges()
      .subscribe(item => {
        this.todoList = [];
        item.forEach(x => {
          const data = x.payload.toJSON();
          data['$key'] = x.key;
          data['state'] = (data['Todo_isImport'] === true) ? 'bring' : 'dark';
          data['contentOpen'] = 'out';
          this.todoList.push(data);
        });

      });
  }
  toggleHelpMenu(item: any): void {
    item.contentOpen = (item.contentOpen === 'out') ? 'in' : 'out';
  }
  ChangeColor(event: any, item: any): void {

    if (item.state === 'dark') {
      event.path[0].style.color = '#F5A623';
      item.state = 'bring';
      this.todoService.UpdateImportant(item.$key, true);

    } else {
      item.state = 'dark';
      event.path[0].style.color = 'gray';
      this.todoService.UpdateImportant(item.$key, false);

    }
  }

  itemCheck(item: any) {
    item.Todo_isCheck = (item.Todo_isCheck === true) ? false : true;
    this.todoService.CheckTitle(item.$key, !item.Todo_isCheck);
  }

  Submit(item: any) {

    if (item.Todo_DeadLine === undefined) {
      alert('請先進行填寫');
    } else {
      this.todoService.UpdateTask(item.$key, item.Todo_DeadLine, item.Todo_Content);
      this.toggleHelpMenu(item);
    }

  }
}
