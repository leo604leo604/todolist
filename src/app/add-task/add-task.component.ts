import { Component, OnInit } from '@angular/core';
import { style, trigger, state, transition, animate } from '@angular/animations';
import { TodoService } from '../service/todo.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        overflow: 'hidden',
        height: '*',
        width: '660px'
      })),
      state('out', style({
        opacity: '0',
        overflow: 'hidden',
        height: '0px',
        width: '0px'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class AddTaskComponent implements OnInit {
  state: String;
  Title: String;
  DeadLine: Date;
  Comment: String;
  saveSuccess: boolean;
  constructor(private todoservice: TodoService) { }

  ngOnInit() {
    this.state = 'out';
    this.saveSuccess = false;
  }
  MakeTask() {
    this.state = (this.state === 'out') ? 'in' : 'out';
  }
  Submit() {

    if (this.Title === undefined || this.DeadLine === undefined) {
        alert('請先進行填寫');
    } else {
      this.todoservice.AddTask(this.Title, this.Comment, this.DeadLine);
      this.saveSuccess = true;
      this.Title = '';
      this.DeadLine = undefined;
      this.Comment = undefined;
      this.state = (this.state === 'out') ? 'in' : 'out';
      setTimeout(() => {
        this.saveSuccess = false;
      }, 1000);
    }

  }
}
