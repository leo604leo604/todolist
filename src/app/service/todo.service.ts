import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { FirebaseOperation } from 'angularfire2/database/interfaces';
@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todoList: AngularFireList<any>;
  constructor(private firebaseDB: AngularFireDatabase) { }

  GetTodoList() {
    this.todoList = this.firebaseDB.list('data');
    return this.todoList;
  }

  AddTask(title: String, content: String, deadline: Date) {
    content = (content === undefined) ? '' : content;
    this.todoList.push({
      Todo_Title: title,
      Todo_Content: content,
      Todo_DeadLine: deadline,
      Todo_isCheck: false,
      Todo_isImport: false
    });
  }

  CheckTitle($key: FirebaseOperation, isCheck: boolean) {

    this.todoList.update($key, { Todo_isCheck: !isCheck });

  }

  UpdateImportant($key: FirebaseOperation, isImportant: boolean) {
    console.log(isImportant);
    this.todoList.update($key, { Todo_isImport: isImportant });

  }

  UpdateTask($key: FirebaseOperation, DeadLine: Date, comment: String) {
    this.todoList.update($key, {
      Todo_DeadLine: DeadLine,
      Todo_Content: comment
    });
  }

  RemoveTitle($key: FirebaseOperation) {
    this.todoList.remove($key);
  }
}
