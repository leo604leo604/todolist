// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBk56tYMlATQHkBo8U4wSSbg40dBYY18VM',
    authDomain: 'todolist-ad2db.firebaseapp.com',
    databaseURL: 'https://todolist-ad2db.firebaseio.com',
    projectId: 'todolist-ad2db',
    storageBucket: 'todolist-ad2db.appspot.com',
    messagingSenderId: '269561794468'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
